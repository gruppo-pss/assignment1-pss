FROM openjdk:8-jdk-slim
ENV TZ=Europe/Rome

RUN apt-get update && apt-get install -y curl

WORKDIR /srv/app/
COPY ./target/homeBrew-0.0.1-SNAPSHOT.jar ./homeBrew.jar

EXPOSE 8085
ENTRYPOINT ["java","-jar","homeBrew.jar"]

