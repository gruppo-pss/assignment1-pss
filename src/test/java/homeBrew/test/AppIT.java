package homeBrew.test;

import homebrew.model.Ingrediente;
import homebrew.model.Nota;
import homebrew.model.TipoAttrezzatura;
import homebrew.model.TipoIngrediente;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.*;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.*;

import homebrew.model.Attrezzatura;
import homebrew.controller.*;
import java.sql.SQLException;
 

/**
 * Test di integrazione.
 */
public class AppIT 
{
	@Test
	public void testDB() throws SQLException{
		ControllerAttrezzatura ca = new ControllerAttrezzatura();
		Attrezzatura a = ca.getStrumento("tubo1");
		assertEquals("tubo1", a.getNome());
		
	}
}