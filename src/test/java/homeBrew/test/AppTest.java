package homeBrew.test;

import homebrew.model.Ingrediente;
import homebrew.model.Nota;
import homebrew.model.TipoAttrezzatura;
import homebrew.model.TipoIngrediente;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.*;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.*;

import homebrew.model.Attrezzatura;
import homebrew.controller.*;
 

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	@Test 
    public void test()
    {
    	Ingrediente i = new Ingrediente("malto", 3.0, false, TipoIngrediente.MALTO);
    	assertFalse(i.isBloccato());
    	
    }
	
	@Test
	public void testAttrezzatura() {
		TipoAttrezzatura tAtr = TipoAttrezzatura.TUBO;
		
		Attrezzatura attr = new Attrezzatura("tubo", 5.0, tAtr);
		Assert.assertEquals(5.0, attr.getPortata(), 0.0);
	}
	
	@Test
	public void testNota() {
		Nota nota = new Nota("Nota1", "Prova di descrizione");
		assertEquals("Messaggio di possibile errore dovuto a:", nota.getDescrizione(), "Prova di descrizione");
	}
	/*
	@Test
	public void testDB() {
		ControllerAttrezzatura ca = new ControllerAttrezzatura();
		Attrezzatura a = ca.getStrumento("tubo1");
		assertEquals("tubo1", a.getNome());
		
	}
    */
}


