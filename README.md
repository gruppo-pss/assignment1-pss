# Brew Day!
Brew Day! è un'applicazione Java, che permette ad ogni birraio casalingo di tenere traccia delle proprie ricette, degli ingredienti disponibili, di 
creare note per ogni ricetta e di ricevere consigli riguardo la migliore ricetta che si può eseguire in un certo momento.
In particolare, questa funzionalità chiamata "what should i brew today?" permette al birraio di conoscere la ricetta che massimizza la quantità 
di birra producibile tra quelle salvate nel database.

## Sviluppo Pipeline

La pipeline è stata sviluppata nel branch developer e una volta conclusa è stato eseguito il merge con il branch Master.
Le fasi implementate nella pipeline sono:

- Build: è stato utilizzato maven per definire le dipendenze e compilare il progetto correttamente.

- Verify: esegue l'analisi statica, tramite il plugin PMD, e l'analisi dinamica del codice tramite il plugin spotbugs. Inoltre per analizzare lo stile del codice è stato usato il plugin checkstyle.

- Unit-test: è stato utilizzato il plugin surefire e tramite junit verifica il corretto funzionamento dei test nella classe AppTest.java. Sono stati testati alcuni metodi delle seguenti classi: Ingrediente, Attrezzatura e Nota.

- Integration-test: è stato utilizzato un database mysql hostato da un servizio esterno. Il test consiste nel connettersi al database remoto, fare una query e verificare la connessione e la correttezza del programma con i dati ottenuti. Il test si trova nella classe AppIT.java.

- Package: viene creato il file .jar del programma, escludendo le classi di testing, utilizzando il plugin Maven Spring-boot e viene salvato nella directory Target.

- Release: viene eseguito il login al Gitlab Container Registry, dopodichè l'immagine Docker contenente il progetto viene estratta dal registry se già presente, costruita tramite l'utilizzo della cache e inserita nuovamente all'interno del Registry una volta modificata.

- Deploy: dopo aver creato una Virtual Machine Unix su Azure si crea un collegamento utilizzando il protocollo SSH ad essa. Per effettuare il collegamento si utilizza la Private_Key, memorizzata in un file temporaneo. Una volta stabilita la connessione viene eseguito il login, utilizzando le credenziali di gitlab, si controlla ed eventualmente rimuove una versione già esistente e si effettua una pull dell'ultima immagine creata. Una volta terminata la fase di pull viene eseguita l'ultima versione. Per testare la correttezza dell'operazione è stato verificato il corretto stato di attivazione dell'applicazione nella VM.


Link alla repository: https://gitlab.com/gruppo-pss/assignment1-pss


Fasi completate nella seconda consegna:
- ~~Package~~
- ~~Release~~
- ~~Deploy~~


### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587 @M-Marchi
- Stoppa Miguel 820506 @mig1214
- Tomasoni Lorenzo 829906 @lTomasoni


### Metodologia di lavoro
Ogni componente ha contribuito a uno studio di gruppo e singolo delle tecnologie e allo sviluppo delle varie fasi. 
Ogni volta che si è verificato un problema è stato effettuato un meeting per trovare una soluzione efficace. I problemi più ostici riscontrati durante lo sviluppo e le relative scelte di implementazione sono state le seguenti:
- Caricamento sull'immagine Docker di un DB già esistente in locale: risolto utilizzando un servizio esterno. (www.freemysqlhosting.net)
- Utilizzo dell'immagine docker sulla VM: risolto reinstallando sulla VM il servizio Docker aggiornato.
